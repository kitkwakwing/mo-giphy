import React, { useState, useEffect } from 'react';
import GifWrapper from '../GifWrapper';
import Search from '../Search';
import Gif from '../Gif';
import './app.css';

const App = () => {
  const limit = 32; //initial total number of images
  const [hasError, setErrors] = useState(false);
  const [offset, setOffset] = useState(limit);
  const [collection, setCollection ] = useState({});
  const gifs = collection.data;

  async function fetchData() {
    const res = await fetch(`http://api.giphy.com/v1/gifs/trending?api_key=j7p37DujWKkfve9e4VeLjYhkS7sEKO1X&limit=${limit}`);
    res
      .json()
      .then(res => setCollection(res))
      .catch(err => setErrors(err));
  }

  async function loadMore() {
    const res = await fetch(`http://api.giphy.com/v1/gifs/trending?api_key=j7p37DujWKkfve9e4VeLjYhkS7sEKO1X&limit=${limit}&offset=${offset}`);
    res
      .json()
      .then(res => setCollection({...collection, res}))
      .then(res => setCollection(res))
      .catch(err => setErrors(err));
      
    setOffset(offset + limit);
  }


  useEffect(() => {
    fetchData();
    const list = document.getElementById('trending-wrapper')
    window.addEventListener('scroll', () => {
      if (window.scrollY + window.innerHeight === list.clientHeight + list.offsetTop) {
        // loadMore();
        console.log('loading more')
      }
    });
  }, []);

  if (hasError) return <h1>Uh oh... there's a problem</h1>;

  return (
    <React.Fragment>
      <Search />
      <h2>Current Trending Gifs, click on the image to see description</h2>
      <GifWrapper id="trending-wrapper">
        {
          gifs && gifs.map(gif => (
            <Gif
              key={gif.id}
              url={gif.images.original.url}
              link={gif.url}
              title={gif.title}
              size={gif.images.original.size} />
          ))
        }
      </GifWrapper>
    </React.Fragment>
  )
};

export default App;
