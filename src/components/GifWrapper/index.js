import React from 'react';
import './gifwrapper.css';

const GifWrapper = props => (
  <div className="giphy-wrapper" id={props.id}>
    {props.children}
  </div>
);

export default GifWrapper;
