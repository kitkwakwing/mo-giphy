import React, { useState } from 'react';
import './gif.css';

const Gif = props => {
  const [description, setDescription ] = useState(false);

  const handleClick = event => setDescription(true);

  return (
    <div className="giphy-item" onClick={() => handleClick()}>
      <img src={props.url} alt="" />
      {description  && (
        <div>
          <p><strong>Title:</strong> {props.title}</p>
          <p><strong>Size:</strong> {props.size}</p>
          <p><strong>Link:</strong> <a href={props.link} target="_blank" rel="noopener noreferrer">{props.link}</a></p>
        </div>
      )}
    </div>
  )
}

export default Gif;
