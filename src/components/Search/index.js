import React, { useState } from 'react';
import GifWrapper from '../GifWrapper';
import Gif from '../Gif';
import './search.css';

const Search = () => {
  const [setErrors] = useState(false);
  const [keyword, setKeyword ] = useState('');
  const [result, setResult ] = useState({});

  const handleSubmit = evt => {
    evt.preventDefault();

    async function submitSearch() {
      const res = await fetch(`https://api.giphy.com/v1/gifs/search?q=${keyword}&api_key=j7p37DujWKkfve9e4VeLjYhkS7sEKO1X&limit=4`);
      res
        .json()
        .then(res => setResult(res))
        .catch(err => setErrors(err));
    }

    submitSearch();
  }

  const { data } = result;

  return (
    <React.Fragment>
      <form className="giphy-search" onSubmit={handleSubmit}>
        <h2>Search</h2>
        <input
          type="search"
          value={keyword}
          onChange={e => setKeyword(e.target.value)}
          />
        <input type="submit" value="submit" />
      </form>
      <GifWrapper>
      {
        data && data.map(gif => (
          <Gif
            key={gif.id}
            url={gif.images.original.url}
            link={gif.url}
            title={gif.title}
            size={gif.images.original.size} />
        ))
      }
      </GifWrapper>
    </React.Fragment>
  )
};

export default Search;